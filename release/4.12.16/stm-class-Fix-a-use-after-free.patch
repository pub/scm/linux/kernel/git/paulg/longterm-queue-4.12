From 33304a544b2318d27ea5e1221e8043ffd9f329ea Mon Sep 17 00:00:00 2001
From: Alexander Shishkin <alexander.shishkin@linux.intel.com>
Date: Tue, 19 Sep 2017 18:47:40 +0300
Subject: [PATCH] stm class: Fix a use-after-free

commit fd085bb1766d6a598f53af2308374a546a49775a upstream.

For reasons unknown, the stm_source removal path uses device_destroy()
to kill the underlying device object. Because device_destroy() uses
devt to look for the device to destroy and the fact that stm_source
devices don't have one (or all have the same one), it just picks the
first device in the class, which may well be the wrong one.

That is, loading stm_console and stm_heartbeat and then removing both
will die in dereferencing a freed object.

Since this should have been device_unregister() in the first place,
use it instead of device_destroy().

Signed-off-by: Alexander Shishkin <alexander.shishkin@linux.intel.com>
Fixes: 7bd1d4093c2 ("stm class: Introduce an abstraction for System Trace Module devices")
Cc: stable@vger.kernel.org
Signed-off-by: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
Signed-off-by: Paul Gortmaker <paul.gortmaker@windriver.com>

diff --git a/drivers/hwtracing/stm/core.c b/drivers/hwtracing/stm/core.c
index 0e731143f6a4..08b8305fee44 100644
--- a/drivers/hwtracing/stm/core.c
+++ b/drivers/hwtracing/stm/core.c
@@ -1119,7 +1119,7 @@ void stm_source_unregister_device(struct stm_source_data *data)
 
 	stm_source_link_drop(src);
 
-	device_destroy(&stm_source_class, src->dev.devt);
+	device_unregister(&src->dev);
 }
 EXPORT_SYMBOL_GPL(stm_source_unregister_device);
 
-- 
2.1.4

