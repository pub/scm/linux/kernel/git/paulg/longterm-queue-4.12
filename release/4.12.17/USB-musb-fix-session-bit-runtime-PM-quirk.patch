From 7aba51769deff3ab98deede4c78886f7064a1624 Mon Sep 17 00:00:00 2001
From: Johan Hovold <johan@kernel.org>
Date: Mon, 9 Oct 2017 22:46:07 -0500
Subject: [PATCH] USB: musb: fix session-bit runtime-PM quirk

commit 4f190e0b9de89c4c917c3ffb3799e9d00fc534ac upstream.

The current session-bit quirk implementation does not prevent the retry
counter from underflowing, something which could break runtime PM and
keep the device active for a very long time (about 2^32 seconds) after a
disconnect.

This notably breaks the B-device timeout case, but could potentially
cause problems also when the controller is operating as an A-device.

Fixes: 2bff3916fda9 ("usb: musb: Fix PM for hub disconnect")
Cc: stable <stable@vger.kernel.org>     # 4.9
Cc: Tony Lindgren <tony@atomide.com>
Signed-off-by: Johan Hovold <johan@kernel.org>
Tested-by: Tony Lindgren <tony@atomide.com>
Signed-off-by: Bin Liu <b-liu@ti.com>
Signed-off-by: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
Signed-off-by: Paul Gortmaker <paul.gortmaker@windriver.com>

diff --git a/drivers/usb/musb/musb_core.c b/drivers/usb/musb/musb_core.c
index 6aae19d8de7b..48ee8a2a94a1 100644
--- a/drivers/usb/musb/musb_core.c
+++ b/drivers/usb/musb/musb_core.c
@@ -1861,22 +1861,22 @@ static void musb_pm_runtime_check_session(struct musb *musb)
 		MUSB_DEVCTL_HR;
 	switch (devctl & ~s) {
 	case MUSB_QUIRK_B_INVALID_VBUS_91:
-		if (musb->quirk_retries--) {
+		if (musb->quirk_retries) {
 			musb_dbg(musb,
 				 "Poll devctl on invalid vbus, assume no session");
 			schedule_delayed_work(&musb->irq_work,
 					      msecs_to_jiffies(1000));
-
+			musb->quirk_retries--;
 			return;
 		}
 		/* fall through */
 	case MUSB_QUIRK_A_DISCONNECT_19:
-		if (musb->quirk_retries--) {
+		if (musb->quirk_retries) {
 			musb_dbg(musb,
 				 "Poll devctl on possible host mode disconnect");
 			schedule_delayed_work(&musb->irq_work,
 					      msecs_to_jiffies(1000));
-
+			musb->quirk_retries--;
 			return;
 		}
 		if (!musb->session)
-- 
2.1.4

