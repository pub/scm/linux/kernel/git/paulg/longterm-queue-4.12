From 85e2d5e49dbfcfafec7605c66c9696b8517b95ff Mon Sep 17 00:00:00 2001
From: Mathias Nyman <mathias.nyman@linux.intel.com>
Date: Tue, 17 Oct 2017 16:07:33 +0300
Subject: [PATCH] usb: hub: Allow reset retry for USB2 devices on connect
 bounce

commit 1ac7db63333db1eeff901bfd6bbcd502b4634fa4 upstream.

If the connect status change is set during reset signaling, but
the status remains connected just retry port reset.

This solves an issue with connecting a 90W HP Thunderbolt 3 dock
with a Lenovo Carbon x1 (5th generation) which causes a 30min loop
of a high speed device being re-discovererd before usb ports starts
working.

[...]
[ 389.023845] usb 3-1: new high-speed USB device number 55 using xhci_hcd
[ 389.491841] usb 3-1: new high-speed USB device number 56 using xhci_hcd
[ 389.959928] usb 3-1: new high-speed USB device number 57 using xhci_hcd
[...]

This is caused by a high speed device that doesn't successfully go to the
enabled state after the second port reset. Instead the connection bounces
(connected, with connect status change), bailing out completely from
enumeration just to restart from scratch.

Link: https://bugs.launchpad.net/ubuntu/+source/linux/+bug/1716332

Cc: Stable <stable@vger.kernel.org>
Signed-off-by: Mathias Nyman <mathias.nyman@linux.intel.com>
Acked-by: Alan Stern <stern@rowland.harvard.edu>
Signed-off-by: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
Signed-off-by: Paul Gortmaker <paul.gortmaker@windriver.com>

diff --git a/drivers/usb/core/hub.c b/drivers/usb/core/hub.c
index 5ea3d1d7bec2..dee18e43fc3f 100644
--- a/drivers/usb/core/hub.c
+++ b/drivers/usb/core/hub.c
@@ -2706,13 +2706,16 @@ static int hub_port_wait_reset(struct usb_hub *hub, int port1,
 	if (!(portstatus & USB_PORT_STAT_CONNECTION))
 		return -ENOTCONN;
 
-	/* bomb out completely if the connection bounced.  A USB 3.0
-	 * connection may bounce if multiple warm resets were issued,
+	/* Retry if connect change is set but status is still connected.
+	 * A USB 3.0 connection may bounce if multiple warm resets were issued,
 	 * but the device may have successfully re-connected. Ignore it.
 	 */
 	if (!hub_is_superspeed(hub->hdev) &&
-			(portchange & USB_PORT_STAT_C_CONNECTION))
-		return -ENOTCONN;
+	    (portchange & USB_PORT_STAT_C_CONNECTION)) {
+		usb_clear_port_feature(hub->hdev, port1,
+				       USB_PORT_FEAT_C_CONNECTION);
+		return -EAGAIN;
+	}
 
 	if (!(portstatus & USB_PORT_STAT_ENABLE))
 		return -EBUSY;
-- 
2.1.4

